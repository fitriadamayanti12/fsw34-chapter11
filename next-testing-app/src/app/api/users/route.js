import { NextResponse } from "next/server";

export async function GET(req) {
  return NextResponse.json([
    {
      id: 1,
      username: "sabrina",
    },
    {
      id: 2,
      username: "john",
    },
    {
      id: 3,
      username: "mike",
    },
  ]);
}
