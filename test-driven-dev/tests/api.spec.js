const request = require("supertest");
const app = require("../app");

describe("Todos API", () => {
  // GET / todos
  test("GET /todos --> array todos", () => {
    return request(app)
      .get("/todos")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              title: expect.any(String),
              completed: expect.any(Boolean),
            }),
          ])
        );
      });
  });

  // GET by id
  test("GET /todos/id --> specific todo by id", () => {
    return request(app)
      .get("/todos/1")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(
          expect.objectContaining({
            title: expect.any(String),
            completed: expect.any(Boolean),
          })
        );
      });
  });

  // If not found
  test("GET /todos/id --> 400 if not found", () => {
    return request(app).get("/todos/9999").expect(404);
  });

  // POST todo
  test("POST /todos --> created todo", () => {
    return request(app)
      .post("/todos")
      .send({
        title: "Coding",
      })
      .expect("Content-Type", /json/)
      .expect(201)
      .then((response) => {
        expect(response.body).toEqual(
          expect.objectContaining({
            title: "Coding",
            completed: false,
          })
        );
      });
  });

  test("POST /todos --> validates request body", () => {
    return request(app).post("/todos").send({ title: 12345 }).expect(422);
  });
});
